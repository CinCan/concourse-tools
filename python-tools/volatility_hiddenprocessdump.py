#!/usr/bin/env python2.7

import sys, subprocess
import datetime
import hashlib

def calculate_hash(filename, buffer_size=65536):
    md5 = hashlib.md5()
    sha1 = hashlib.sha1()
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
    while True:
        data = f.read(buffer_size)
        if not data:
            break
        md5.update(data)
        sha1.update(data)
        sha256.update(data)
    hashes = {}
    hashes{"md5": md5.hexdigest()}
    hashes{"sha1": sha1.hexdigest()}
    hashes{"sha256": sha256.hexdigest()}
    return hashes


def connections(sample, profile, pID):
    params = ["-f", sample, "--profile=" + profile]
    output = ""

    winXP2003 = [
        "WinXPSP1x64",
        "WinXPSP2x64",
        "WinXPSP2x86",
        "WinXPSP3x86",
        "Win2003SP0x86",
        "Win2003SP1x64",
        "Win2003SP1x86",
        "Win2003SP2x64",
        "Win2003SP2x86"
        ]
    win2008Vista7 = [
        "Win2008R2SP0x64",
        "Win2008R2SP1x64",
        "Win2008SP1x64",
        "Win2008SP1x86",
        "Win2008SP2x64",
        "Win2008SP2x86",
        "Win7SP0x64",
        "Win7SP0x86",
        "Win7SP1x64",
        "Win7SP1x86",
        "VistaSP0x64",
        "VistaSP0x86",
        "VistaSP1x64",
        "VistaSP1x86",
        "VistaSP2x64",
        "VistaSP2x86"]

    if (profile in winXP2003):
        connections_output = subprocess.check_output(["vol.py", "connections"] + params,
         universal_newlines=True)
        connscan_output = subprocess.check_output(["vol.py", "connscan"] + params,
         universal_newlines=True)
        sockets_output = subprocess.check_output(["vol.py", "sockets"] + params,
         universal_newlines=True)
        sockscan_output = subprocess.check_output(["vol.py", "sockscan"] + params,
         universal_newlines=True)

        output += "connections:\n{}\n".format(connections_output)
        output += "connscan:\n{}\n".format(connscan_output)
        output += "sockets:\n{}\n".format(sockets_output)
        output += "sockscan:\n{}\n".format(sockscan_output)

    elif (profile in win2008Vista7):
        netscan_output = subprocess.check_output(["vol.py", "netscan", "-f"] + params,
         universal_newlines=True)
        output += "netscan:\n{}\n".format(netscan_output)

    return output

def hidden_processes(sample):
    # find all processes not visible to end user
    h_output = subprocess.check_output(["vol.py", "psxview", "-f", sample],
         universal_newlines=True)
    result = h_output.split("\n")

    hidden = []
    for i in result[2:]:
        process = i.split()
        try:
            if (process[4]=="False"):
                hidden.append([process[0], process[2]])
        except IndexError:
            pass

    return hidden, h_output

def get_profile(sample):
    # get the memory sample profile for the sample
    pr_output = subprocess.check_output(["vol.py", "kdbgscan", "-f", sample],
         universal_newlines=True)
    
    result = pr_output.split("\n")

    suggested_profiles = []
    for i in result:
        if ("Profile suggestion" in i):
            suggested_profiles.append(i.split(": ")[1])
    
    return suggested_profiles, pr_output

def dump_process(sample, output_path, pID, offset, profile):
    dump = subprocess.Popen(["vol.py", "procdump", "-D",
        output_path, "-f", sample, "-p", pID, "--offset=" + offset,
         "--profile=" + profile],
         universal_newlines=True)
    dump.wait()
    return hashes(sample)

def main():
    sample = sys.argv[1]
    output_path = sys.argv[2]
    hidden, h_output = hidden_processes(sample)
    profile, pr_output = get_profile(sample)
    for process in hidden:
        dump_process(sample, output_path, process[1], process[0], profile[0])
    conns = connections(sample, profile[0], [1])

    filename = datetime.date.today().strftime("%m-%d-%Y")
    with open("{}/{}.txt".format(output_path, filename), "w") as out:
        out.write("kdbgscan\n\n")
        out.write(pr_output)
        out.write("psxview\n\n")
        out.write(h_output)
        out.write(conns)

if __name__=="__main__":
    main()