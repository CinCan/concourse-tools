#!/usr/bin/env python2.7

import sys, os, subprocess
import json
import hashlib


def calculate_hash(filename, buffer_size=65536):
    md5 = hashlib.md5()
    sha1 = hashlib.sha1()
    sha256 = hashlib.sha256()

    with open(filename, 'rb') as f:
        while True:
            data = f.read(buffer_size)
            if not data:
                break
            md5.update(data)
            sha1.update(data)
            sha256.update(data)
        hashes = {}
        hashes["md5"] = md5.hexdigest()
        hashes["sha1"] = sha1.hexdigest()
        hashes["sha256"] = sha256.hexdigest()
    hashes["filename"] = filename
    return hashes


def search_array(term, column, array):
    output = {"columns": array["columns"], "rows": []}
    try:
        i = array["columns"].index(column)

        for row in array:
            if row[i] == term:
                output["rows"].append(row)
        return output

    except ValueError:
        return None


def connections(sample, profile):
    params = ["-f", sample, "--profile=" + profile, "--output=json"]
    output = {}

    winXP2003 = [
        "WinXPSP1x64",
        "WinXPSP2x64",
        "WinXPSP2x86",
        "WinXPSP3x86",
        "Win2003SP0x86",
        "Win2003SP1x64",
        "Win2003SP1x86",
        "Win2003SP2x64",
        "Win2003SP2x86"
        ]
    win2008Vista7 = [
        "Win2008R2SP0x64",
        "Win2008R2SP1x64",
        "Win2008SP1x64",
        "Win2008SP1x86",
        "Win2008SP2x64",
        "Win2008SP2x86",
        "Win7SP0x64",
        "Win7SP0x86",
        "Win7SP1x64",
        "Win7SP1x86",
        "VistaSP0x64",
        "VistaSP0x86",
        "VistaSP1x64",
        "VistaSP1x86",
        "VistaSP2x64",
        "VistaSP2x86"]

    if (profile in winXP2003):
        connections_output = subprocess.check_output(["vol.py", "connections"] + params,
         universal_newlines=True)
        connscan_output = subprocess.check_output(["vol.py", "connscan"] + params,
         universal_newlines=True)
        sockets_output = subprocess.check_output(["vol.py", "sockets"] + params,
         universal_newlines=True)
        sockscan_output = subprocess.check_output(["vol.py", "sockscan"] + params,
         universal_newlines=True)

        output["connections"] = json.loads(connections_output)
        output["connscan"] = json.loads(connscan_output)
        output["sockets"] = json.loads(sockets_output)
        output["sockscan"] = json.loads(sockscan_output)

    elif (profile in win2008Vista7):
        netscan_output = subprocess.check_output(["vol.py", "netscan", "-f"] + params,
         universal_newlines=True)
        netscan_output = json.loads(netscan_output)

        output["netscan"] = netscan_output

    return output


def hidden_processes(sample, profile):

    params = ["-f", sample,
              "--profile=" + profile,
              "--output=json"]

    result = subprocess.check_output(["vol.py", "psxview"] + params, universal_newlines=True)

    processes = json.loads(result)
    hidden = []
    for row in processes["rows"]:
        if ('False' in row[4]):
            hidden.append([str(row[0]), str(row[2])])
    return result, hidden

def get_profile(sample):
    result = subprocess.check_output(["vol.py", "kdbgscan", "-f", sample], universal_newlines=True)
    result = result.split("\n")
    suggested_profiles = []
    for i in result:
        if ("Profile suggestion" in i):
            suggested_profiles.append(i.split(": ")[1])
    return suggested_profiles

def dump_process(sample, output_path, offset, pID, profile):
    param = ["-D", output_path,
             "-f", sample,
             "-p", str(pID),
             "--offset=" + str(offset),
             "--profile=" + profile
             ]
    print output_path, pID, type(offset), type(pID)
    dump = subprocess.Popen(["vol.py", "procdump"] + param, stdout=subprocess.PIPE)
    dump.wait()

def main():
    sample = sys.argv[1]
    output_path = sys.argv[2]

    output = {}

    output["memory profile"] = get_profile(sample)[0]
    output["processes"], output["hidden processes"] = hidden_processes(sample, output["memory profile"])
    output["connections"] = connections(sample, output["memory profile"])

    for process in output["hidden processes"]:
        dump_process(sample, output_path, process[0], process[1], output["memory profile"])

    output["hashes"] = []
    for filename in os.listdir(output_path):
        if filename.endswith(".exe"):
            output["hashes"].append(calculate_hash(output_path + "/" + filename))

    with open(output_path + "/volatility.json", "w") as outfile:
        print "Saving output to {}/volatility.json".format(output_path)
        json.dump(output, outfile, indent=2)

if __name__=="__main__":
    main()